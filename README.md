# Databse table backup and restore for Laravel
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.2-blue.svg?cacheSeconds=2592000" />
</p>

### This package backs-up and restores the database.
Backups are stored in the configured storage folder as: backup/database/YYYYMMDD/table_name.json


### Install

```sh
composer require nomercy/backup
```

### Run one of these commands.

```php
php artisan nomercy:backup --backup # backs up tables set in the config/backup.php
php artisan nomercy:backup --backup --all # backs up all tables.
php artisan nomercy:backup --restore # restores all non existing records.
php artisan nomercy:backup --restore --force # deletes the records before inserting.
```

### .env configuration.

Set the ```BACKUP_TABLES``` to 'all' sets the --all parameter by default, defaults to config.tables<br>
Set the ```BACKUP_DISK``` to choose a storage config path, defaults to local.<br>
Set the ```BACKUP_LIMIT``` to limit or increase the max stored backups, defaults to 14.

## Author

### 👤 **Stoney_Eagle**

* Twitter: [@Stoney_Eagle](https://twitter.com/Stoney_Eagle)
* Gitlab: [@Stoney_Eagle](https://gitlab.com/Stoney_Eagle)
