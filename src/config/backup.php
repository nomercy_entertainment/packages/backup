<?php

return [

    "disk" => env("BACKUP_DISK", 'local'),
    "limit" => env("BACKUP_LIMIT", 14),
    "type" => env('BACKUP_TABLES', 'tables'), # all or tables listed below

    "tables" => [
        'user_videos',
        'supported_languages',
        'server',
        'progress',
        'stream',
        'special',
    ],
];
