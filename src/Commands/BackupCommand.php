<?php

namespace NoMercy\Backup\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BackupCommand extends Command
{
    protected $signature = 'nomercy:backup

    {--b|backup : Backup data to storage}
    {--a|all : Backup every table to storage}

    {--r|restore : Restore data from storage}
    {--f|force : Force restore deleted all items first}';

    protected $description = 'Backup custom database record that are manually made';

    public function __construct()
    {
        parent::__construct();

        $this->disk = config('backup.disk');
    }

    public function handle()
    {
        if (!$this->option('backup') && !$this->option('restore')) {
            $this->comment('You need to specify a action.');
            return;
        }
        if ($this->option('backup')) {
            $this->comment('Backing up data...');
            $this->backup();
            $this->comment('Done.');
            return;
        }

        if ($this->option('restore')) {
            $this->comment('Restoring data..');
            $this->restore($this->option('force'));
            $this->comment('Done.');
            return;
        }
    }

    public function limit_backups(){
        $i = 1;
        $keep = config('backup.limit');
        $folders = Storage::disk($this->disk)->directories('/backup/database/');

        foreach(collect($folders)->reverse() as $key => $folder){
            if($i >= ($keep +1)){
                $this->comment('deleting: '. $folder);
                Storage::disk($this->disk)->deleteDirectory($folder);
                unset($folders[$key]);
            }
            $i++;
        }
    }

    public function get_tables(){
        $tables = [];
        if(config('backup.type') == 'all' || $this->option('all')){
            $db = DB::select('SHOW TABLES');
            $name = 'Tables_in_'. env('DB_DATABASE');
            foreach($db as $table)
            {
                $tables[] = $table->$name;
            }
        }
        else{
            $tables = config('backup.tables');
        }
        return $tables;
    }

    public function get_folders(){
        $i = 1;
        $existing = [];
        $folders = Storage::disk($this->disk)->directories('/backup/database/');

        foreach(collect($folders)->reverse() as $folder){
            $existing[] = explode('/', $folder)[count(explode('/', $folder)) -1 ];
        }
        return $folders;
    }

    public function restore($force = 0){
        $folders = $this->get_folders();
        $lastFolder = $folders[count($folders) - 1];
        $files = Storage::disk($this->disk)->files($lastFolder);

        foreach($files as $file){
            $query = 0;
            $table = str_replace('.json', '', explode('/', $file)[count(explode('/', $file)) -1 ]);
            $content = json_decode(Storage::disk($this->disk)->get($file), true);
            if(count($content) != 0){
                if($force){
                    DB::table($table)->delete();
                }
                foreach (array_chunk($content, 2000)  as $key => $t) {
                    $query = DB::table($table)->insertOrIgnore($t);
                    if($query){
                        $this->comment('Restored table: ' . $table . ' part: ' . $key);
                    }
                }
            }
        }
    }

    public function backup(){
        foreach($this->get_tables() as $table){
            $data = DB::table($table)->get();
            Storage::disk($this->disk)->put('/backup/database/' . date("Ymd"). '/' . $table . '.json', json_encode($data));
            $this->comment('Backed-up table: ' . $table);
        }
    }
}
