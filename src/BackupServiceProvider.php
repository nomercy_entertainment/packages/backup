<?php

namespace NoMercy\Backup;

use Illuminate\Support\ServiceProvider;
use NoMercy\Backup\Commands\BackupCommand;

class BackupServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/backup.php',
            'backup'
        );

        if ($this->app->runningInConsole()) {
            $this->commands([
                BackupCommand::class,
            ]);
        }
    }

    public function register()
    {
        $this->publishes([
            __DIR__ . '/config/backup.php' => config_path('backup.php'),
        ]);

        $this->commands([
            BackupCommand::class,
        ]);
    }
}
